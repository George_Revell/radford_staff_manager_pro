﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace RadfordStaffManager.Models
{
    public class Employee
    {
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string MiddleInitial { get; set; }
        public string LastName { get; set; }

        [DataType(DataType.PhoneNumber)]
        public string MobilePhone { get; set; }

        [DataType(DataType.PhoneNumber)]
        public string OfficeExtention { get; set; }
        // [StringLength(9)]
        //[MaxLength(9)]
        //[MinLength(8)]
        public int IRDnumber { get; set; }
        public bool IsActive { get; set; }

    }
}

