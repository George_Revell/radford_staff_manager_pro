﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using RadfordStaffManager.Models;

namespace Radford_Staff_Manager.Data
{
    public class Radford_Staff_ManagerContext : DbContext
    {
        public Radford_Staff_ManagerContext (DbContextOptions<Radford_Staff_ManagerContext> options)
            : base(options)
        {
        }

        public DbSet<RadfordStaffManager.Models.Employee> Employee { get; set; }
    }
}
